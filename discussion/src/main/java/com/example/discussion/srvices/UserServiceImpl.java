package com.example.discussion.srvices;

import com.example.discussion.models.Post;
import com.example.discussion.models.User;
import com.example.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    public void createUser(User user) {
        userRepository.save(user);
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public ResponseEntity deleteUser(int id) {
        userRepository.deleteById(id);
        return new ResponseEntity("User deleted successfully", HttpStatus.OK);
    }

    public ResponseEntity updateUser(int id, User user) {
        User userForUpdate = userRepository.findById(id).get();
//        update username
        userForUpdate.setUsername((user.getUsername()));
//        update password
        userForUpdate.setPassword((user.getPassword()));

        userRepository.save(userForUpdate);

        return new ResponseEntity("User updated successfuly", HttpStatus.OK);

    }



}
