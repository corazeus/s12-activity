package com.example.discussion.srvices;

import com.example.discussion.models.Post;
import com.example.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

//    create user
    void createUser(User user);

//    get all users
    Iterable<User> getUsers();

    //    delete a user
    ResponseEntity deleteUser(int id);

    //    update a user
    ResponseEntity updateUser(int id, User user);


}
