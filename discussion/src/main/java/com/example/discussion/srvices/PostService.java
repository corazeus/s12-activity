package com.example.discussion.srvices;

import com.example.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(Post post);

    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post post);
}
