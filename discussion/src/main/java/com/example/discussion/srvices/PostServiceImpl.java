package com.example.discussion.srvices;

import com.example.discussion.models.Post;
import com.example.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
//The @Service annotation will allow us to use the CRUD methods inhereted from the CRUD repository.
//even though the interfaces do not contain implementation/method bodies.
public class PostServiceImpl implements PostService{

    //@Autowaired indicates that Spring should automatically inject and instance of the PostRepository into the postRepository variable.
    @Autowired
    private PostRepository postRepository;

    //Create Post
    public void createPost(Post post) {
        postRepository.save(post);
    }

    //Get All Posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //Delete Post by ID
    public ResponseEntity<Object> deletePost(Long id) {
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post Deleted Successfully!", HttpStatus.OK);
    }

    //Update Post
    public ResponseEntity<Object> updatePost(Long id, Post post){
        Post postToUpdate = postRepository.findById(id).get();

        postToUpdate.setTitle(post.getTitle());
        postToUpdate.setContent(post.getContent());

        postRepository.save(postToUpdate);
        return new ResponseEntity<>("Post updated successfully!", HttpStatus.OK);
    }
}
