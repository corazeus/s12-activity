package com.example.discussion.controllers;

import com.example.discussion.models.Post;
import com.example.discussion.srvices.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//Handles the incoming HTTP requests in the map then to the corresponding methods in the controller.
@RestController
//CORS - Cross Origin Resource Sharing
//Allows request from a different domain to be made to the application
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //Create new Post
    @PostMapping("/posts")
    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postService.createPost(post);
        return new ResponseEntity<>("Post Created Succesfully", HttpStatus.CREATED);
    }

    //Get all Posts
    @GetMapping("/posts")
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //Delete Post by ID as PathVariable
    @DeleteMapping("/posts/{postid}")
    public ResponseEntity<Object> deletePost(@PathVariable Long postid){
        return postService.deletePost(postid);
    }

    //Uodate Post by ID as variable
    @PutMapping("/posts/{postid}")
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestBody Post post){
        return postService.updatePost(postid, post);
    }

}
