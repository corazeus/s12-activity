package com.example.discussion.controllers;

import com.example.discussion.models.User;
import com.example.discussion.srvices.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

//    Create User
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        userService.createUser(user);

        return new ResponseEntity<>("User successfully created.", HttpStatus.CREATED);
    }
//    Get all users
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public  ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

//    update user by id
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable int userid, @RequestBody User user) {
        return userService.updateUser(userid, user);
    }

//    delete user by id
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable int userid) {
        return userService.deleteUser(userid);
    }
}
